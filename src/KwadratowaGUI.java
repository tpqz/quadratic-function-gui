/**
 * Created by Mateusz on 07.05.2016.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.*;

class Container extends JPanel{

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    JLabel aL  = new JLabel("Parametr a");
    JTextField a = new JTextField(2);

    JLabel bL  = new JLabel("Parametr b");
    JTextField b = new JTextField(2);

    JLabel cL  = new JLabel("Parametr c");
    JTextField c = new JTextField(2);

    JButton exec = new JButton("Oblicz");

    JTextArea screen = new JTextArea(4,20);


    public Container(){

        SpringLayout layout = new SpringLayout();
        setLayout(layout);

        add(aL);
        add(a);

        layout.putConstraint(SpringLayout.WEST, aL, 5, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, aL, 10, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, a, 30, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, a, 20, SpringLayout.EAST, new JLabel(""));

        add(bL);
        add(b);

        layout.putConstraint(SpringLayout.WEST, bL, 80, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, bL, 10, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, b, 30, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, b, 95, SpringLayout.EAST, new JLabel(""));

        add(cL);
        add(c);

        layout.putConstraint(SpringLayout.WEST, cL, 155, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, cL, 10, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, c, 30, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, c, 170, SpringLayout.EAST, new JLabel(""));

        add(exec);

        layout.putConstraint(SpringLayout.WEST, exec, 78, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, exec, 70, SpringLayout.NORTH, new JLabel(""));

        add(screen);
        screen.setEditable(false);

        layout.putConstraint(SpringLayout.WEST, screen, 5, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, screen, 110, SpringLayout.NORTH, new JLabel(""));

        Event event = new Event();
        exec.addActionListener(event);
    }

    class Event implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                String aString = a.getText();
                double A = Double.parseDouble(aString);

                String bString = b.getText();
                double B = Double.parseDouble(bString);

                String cString = c.getText();
                double C = Double.parseDouble(cString);

                double delta = Math.pow(B, 2) - 4 * A * C;
                double x0, x1, x2, p, q;

                p = -B / (2 * A);
                q = -delta / (4 * A);

                screen.setText("Delta = " + delta + "\n");

                if (delta > 0) {
                    x1 = ((-B) - Math.sqrt(delta)) / (2 * A);
                    x2 = ((-B) + Math.sqrt(delta)) / (2 * A);

                    screen.setText("Delta = " + delta + "\n" +
                            "x1= " + round(x1,2) + " x2= " + round(x2,2) + "\n" + "p= " + round(p,2) + " q= " + round(q,2));
                } else if (delta == 0) {
                    x0 = (-B) / 2 * A;
                    screen.setText("Delta = " + delta + "\n" +
                            "x0=" + round(x0,2) + "p= " + round(p,2) + " q= " + round(q,2));
                } else if (delta < 0) {
                    screen.setText("Delta = " + delta + "\n" +
                            "Brak pierwiastkow\n" + "p= " + round(p,2) + " q= " + round(q,2));

                }

            }catch (java.lang.NumberFormatException e){
                screen.setText("Podaj poprawne parametry!\n");
            }
        }

    }
}


public class KwadratowaGUI {
    public static void main(String[] args) {

        final JFrame okno = new JFrame("Kwadratowa");
        okno.add(new Container());
        okno.setVisible(true);
        okno.setSize(235,210);
        okno.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        okno.setResizable(false);
    }
}
